# Parte 2.1 - Escreva abaixo sua função impares_consecutivos(n)

function impares_consecutivos(n)
    # escreva sua solução aqui
end

# Parte 2.2 - Escreva abaixo suas funções imprime_impares_consecutivos(m) e mostra_n(n)

function imprime_impares_consecutivos(m)
    # escreva sua solução aqui
end

function mostra_n(n)
    # escreva sua solução aqui
end

# Testes automatizados - segue os testes para a parte 2.1. Não há testes para a parte 2.2.

function test()
    if impares_consecutivos(1) != 1
        print("Sua função retornou o valor errado para n = 1")
    end
    if impares_consecutivos(2) != 3
        print("Sua função retornou o valor errado para n = 2")
    end
    if impares_consecutivos(7) != 43
        print("Sua função retornou o valor errado para n = 7")
    end
    if impares_consecutivos(14) != 183
        print("Sua função retornou o valor errado para n = 14")
    end
    if impares_consecutivos(21) != 421
        print("Sua função retornou o valor errado para n = 21")
    end
end

# Para rodar os testes, certifique-se de que suas funções estão com os nomes corretos! Em seguida, descomente a linha abaixo:
# test()


function encontra_multiplo_do_ultimo_impa(n)
   num = n^(3)
   
   indicador = 0
   
   
   for i in 0:num
      
      if i%2 != 0
         
         indicador = indicador + 1
         if indicador == n
         
            return i
         
         end
      end
   end
   
end




function soma_numeros(numimpa,parada)
   
   soma = 0
   i = numimpa
   
   while i >= parada
      
      soma = soma + i
      i = i - 2
   
   end

   return soma
end





function encontra_sequencia(mult,n)
   
   resul = n^(3)
   seqencia = mult
   
   while seqencia < resul
   
      seqencia = soma_numeros(mult,(mult - (2*(n-1))))
      
      mult = mult + 2
      
   end


   return mult - 2
end





function impares_consecutivos(n)
   
   E = encontra_multiplo_do_ultimo_impa(n)
   
   M = encontra_sequencia(E,n)

   if n == 1
      return 1
   end
   
   return M - (2*(n-1))
   
end





function imprime_impares_consecutivos(m)
   
   E = encontra_multiplo_do_ultimo_impa(m)

   M = encontra_sequencia(E,m)

   if m == 1
      e = [1 1 1]
      return e
   end

   primeiro = (M - (2*(m-1)))
   
   z = [0 0]
   i = 1   

   while i <= m
      
      z = [z primeiro]
      primeiro = primeiro + 2
      i = i + 1   
   end
   
   z[1] = m
   z[2] = m^3
   
   return z
end





function mostra_n(n)
   
   resultado = []
   i = 1
   while i <= n
   
      e = imprime_impares_consecutivos(i)
      println(e)

      i = i + 1
      
   end
end
